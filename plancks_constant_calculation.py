"""Compute Planck's constant from the entropy derived from calorimetry
"""

import numpy as np
import scipy.constants as sc

m = 40e-3 / sc.N_A
kt = sc.k * 85.67
s_from_calorimetry = 130.0
p = 0.937 * 1.0e5

h = (np.sqrt(2 * np.pi * m) * (kt)**(5.0/6.0) / p**(1.0/3.0)
     * np.exp(1.0 / 3.0 * (2.5 - s_from_calorimetry/sc.R)))

print("Planck's constant (J s):", h)
