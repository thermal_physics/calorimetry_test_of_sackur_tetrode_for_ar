source := $(shell basename `pwd`)

$(source)_generated.pdf : $(source).py $(source).mplstyle
	python3 prepare_csv_for_distribution.py
	python3 $< 
