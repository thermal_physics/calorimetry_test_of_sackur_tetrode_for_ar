"""Plot entropies versus temperature"""

import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as si
import entropy_calc
import ar_entropy_as_function_of_temperature as ae

def plot():
    ts, es = ae.ar_entropy_as_a_function_of_temperature()
    print("highest T entropy: (J/K) ", es[-1])

    ts_st = np.linspace(0.1,100,100)
    es_st = entropy_calc.st_entropy(
        **dict(t = ts_st, p = 0.937*1.0e5, mm = 39.95))

    plt.ion()
    base_filename = os.path.splitext(__file__)[0]
    with plt.style.context(base_filename + ".mplstyle"):
        fig = plt.figure()
        ax = fig.add_axes([0.2, 0.15, 0.77, 0.80])
        ax.set_xlim([0,100])
        ax.set_ylim([0,150])
        ax.plot(ts, es, label="from calorimetry data")
        ax.plot(ts_st, es_st, "-.", label="Sackur-Tetrode")
        ax.axhline(es[-1], 0.7, 1.0, color="black", alpha=0.5)
        ax.set_ylabel(r"$S \: (\SI{}{J/K})$", rotation=0.0, labelpad=20)
        ax.set_xlabel(r"$T \: (\SI{}{K})$")
        ax.legend(loc="upper left")

        ax.text(40, 30, "heating solid")
        ax.text(70, 45, "melting")
        ax.text(90, 50, "heating\nliquid")
        ax.text(65, 90, "vaporization")
        plt.savefig(base_filename + "_generated.pdf")


if __name__ == "__main__":
    plot()

