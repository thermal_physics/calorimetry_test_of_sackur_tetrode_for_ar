Testing Sackur-Tetrode with calorimetry data
============================================

Python code to answer a question from Fall 2020, Phys 358 Test 4, University of Waterloo.

The calorimetry data is from: https://dx.doi.org/c8qtdv

First run:
[prepare_csv_for_distribution.py](prepare_csv_for_distribution.py)
to get a file with the heat capacity data in the correct units.

To determine Sackur-Tetrode entropies:
[entropy_calc.py](entropy_calc.py)

To determine entropies from alorimetry, making a plot comparing with Sackur-Tetrode:

[calorimetry_test_of_sackur_tetrode_for_ar.py](calorimetry_test_of_sackur_tetrode_for_ar.py)

Assuming Sackur-Tetrode is true, compute Planck's constant using calorimetry data:
[plancks_constant_calculation.py](plancks_constant_calculation.py)


