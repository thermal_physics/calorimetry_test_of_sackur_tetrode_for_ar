"""Sackur-Tetrode entropy function and some calculations"""

import numpy as np
import scipy.constants as sc

def st_entropy(*, p, t, mm):
    """Compute the entropy (J/K) of 1 mol of a monatomic gas using the
       Sackur-Tetrode equation.

       p: Pascal
       t: Kelvin
       mm: molar mass (g)"""
    m = mm * 1e-3 / sc.N_A
    arg_ln = 1.0/p*(2*np.pi*m/sc.h**2)**(3.0/2.0)*(sc.k*t)**(5/2)
    s = sc.k * sc.N_A *(np.log(arg_ln)+5.0/2.0)
    return s

if __name__ == "__main__":
    # check value for Argon, i.e., SITP Problem 2.33
    mm_ar = 39.95
    s_argon = st_entropy(p=1.0e5, t=300.0, mm=mm_ar)
    print("SITP Problem 2.33 S (J/K) : ", s_argon)
    assert np.isclose(s_argon, 155.0, rtol=1e-3)

    # value for Argon, appropriate for calorimetric data
    ar_conditions = dict(
        t = 85.67,
        p = 0.937*1.0e5,
        mm = mm_ar
        )
    print(f"Under conditions: {ar_conditions}")
    print("Entropy of Ar:")
    print(st_entropy(**ar_conditions))

    h2o_conditions = dict(
        t = 300.0,
        p = 1.0e5,
        mm = 18.0)
    print(f"Under conditions: {h2o_conditions}")
    print("Entropy of water (falsely) assuming its monatomic:")
    print(st_entropy(**h2o_conditions))
