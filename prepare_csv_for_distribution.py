"""Take heat capacities from Table 1 of Flubacher et al., https://doi.org/c8qtdv
and convert to metric units and extrapolate to make the data reach the melting
point temperature
"""

import csv
import numpy as np

with open('flubacher_shortdoi_c8qtdv.csv', newline="") as f:
    reader = csv.reader(f)
    rows = [[float(c) for c in row]
            for row in reader if row[0] != "#"]
    first_row = rows[0]
    ts, ccals = (np.array(column) for column in
                ([row[column_index] for row in rows]
                for column_index, _ in enumerate(first_row)))

# append "eyeballed" extrapolation:
ts = np.append(ts, 83.8)
ccals = np.append(ccals, 8.5)

cal_to_joules = 4.184
cjs = ccals * cal_to_joules

with open('ts_and_cs_generated.csv', "w") as f:
    f.write("# T(K), C (J/K) for 1 mol (from https://dx.doi.org/c8qtdv )\n")
    writer = csv.writer(f)
    for r in zip(ts, cjs):
        writer.writerow(r)

