import csv
import numpy as np
import scipy.integrate as si

def ar_entropy_as_a_function_of_temperature():
    """Compute entropy of 1 mol of Ar as a function of temperature using
    calorimetry data from Flubacher et al., https://dx.doi.org/c8qtdv

    Return two arrays:  temperatures (K) and the corresponding entropies (J/K)
    """

    t_m = 83.8  # melting temperature (K)
    l_m = 1190.0  # latent heat of melting (J)
    t_v = 85.67  # vaporization temperature (K)
    l_v = 6544.0  # latent heat of vaporization (J)
    c_l = 43.932  # heat capacity of liquid (J/K)

    # read in heat capacity data for solid from the *.csv file:
    with open('ts_and_cs_generated.csv', newline='') as f:
        reader = csv.reader(f)
        raw_rows = [r for r in reader]
        rows = [[float(c) for c in row] for row in raw_rows[1:]]
        first_row = rows[0]
        ts, cjs = (np.array(column) for column in
                   ([row[column_index] for row in rows]
                    for column_index, _ in enumerate(first_row)))

    part_a_ts = ts
    part_a_ss = np.zeros_like(ts)
    part_a_ss[0] = 0.0  # the third law!

    # integrate C/T for the solid:
    for i in range(1, len(part_a_ts)):
        part_a_ss[i] = si.simps(cjs[0:i] / ts[0:i] , ts[0:i])

    # add L_m/T for melting and integrate C/T for liquid:
    part_b_ts = np.linspace(t_m, t_v, 10)
    part_b_ss = part_a_ss[-1] + l_m / t_m + np.zeros_like(part_b_ts)
    part_b_ss += c_l * np.log(part_b_ts/ t_m)

    # add L_v/T for vaporization:
    part_c_ts = np.array([t_v,])
    part_c_ss = np.array([part_b_ss[-1] + l_v / t_v,])

    return (np.concatenate([part_a_ts, part_b_ts, part_c_ts]),
            np.concatenate([part_a_ss, part_b_ss, part_c_ss]))
